<?php

/**
 * @file
 * theme-settings.php
 *
 * Provides theme settings.
 *
 * @see ./includes/settings.inc
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\Exception\RequestException;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function menuistic_form_system_theme_settings_alter(&$form, FormStateInterface $form_state, $form_id = NULL) {
  $base_theme_path = drupal_get_path('theme', 'mbase');
  if (!$base_theme_path) {
    $form = array();
    $form['nombase'] = array(
      '#type' => 'markup',
      '#title' => t('Error'),
      '#markup' => '<div class = "nombasetheme">This theme depend on
        <strong><a href = "https://www.drupal.org/project/mbase" target = "_blank">mbase</a></strong>
        base theme. Please install <strong><a href = "https://www.drupal.org/project/mbase" target = "_blank">
        mbase</a></strong> theme first. Please read the readme.txt file.</div>',
    );
    unset($form['olor_scheme_form']);
  }
  else {
    include_once $base_theme_path . '/includes/helper.inc';
    $regions = array(
      'home_welcome' => t('Homepage Hero'),
    );
    $home_page_settings = _mbase_homepage_settings($regions, 'menuistic');
    $form = $form + $home_page_settings;

    $form['menuisticgroup'] = array(
      '#type' => 'details',
      '#title' => t('Menuistic Settings'),
      '#group' => 'mbase',
    );

    $menus = menu_ui_get_menus();
    $default_menu = _mbase_setting('menu_menu', $theme, NULL);
    $form['menuisticgroup']['menu_menu'] = array(
      '#type' => 'select',
      '#title' => t('Select the Menu'),
      '#options' => $menus,
      '#description' => t('Select the menu to display in home page.'),
      '#default_value' => $default_menu ? $default_menu : 'main',
    );

    $default_level = _mbase_setting('menu_level', $theme, NULL);
    $form['menuisticgroup']['menu_level'] = array(
      '#type' => 'select',
      '#title' => t('Select the Menu Depth'),
      '#options' => array(
        0 => 'All',
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => '10',
      ),
      '#description' => t('Select the menu Depth. by default, its 3'),
      '#default_value' => $default_level ? $default_level : 3,
    );

    $menu_other_settings = array(
      'show_description' => 'Show the Menu item Description',
      'hide_orphan_tree' => 'Hide if an Level 1 menu item have no child',
    );
    $default_other_settings = _mbase_setting('menu_other_settings', $theme, NULL);
    $form['menuisticgroup']['menu_other_settings'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Other Settings'),
      '#description' => t('Select this to show the menu item description'),
      '#options' => $menu_other_settings,
      '#default_value' => $default_other_settings,
    );
  }
}
