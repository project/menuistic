<?php

/**
 * @file
 * Theme file.
 *
 * All preprocessors and theme functions goes here.
 */

use Drupal\Core\Menu\MenuTreeParameters;

/**
 * Implements hook_preprocess_page().
 *
 * @see page.html.twig
 */
function menuistic_preprocess_page(&$variables) {
  $theme = 'menuistic';

  $home_page_regions = array('home_welcome');

  $variables['snippet'] = _mbase_smart_render($home_page_regions, $theme);

  // We need this only for home page.
  if (\Drupal::service('path.matcher')->isFrontPage()) {

    // Get the menu name.
    $menu_name = _mbase_setting('menu_menu', $theme, NULL);
    $menu_name = $menu_name ? $menu_name : 'main';

    $menu_tree = _menuistic_get_cached_menu_tree($menu_name);
    uasort($menu_tree['#items'], "_menuistic_weight_cmp");
    $variables['contentmenu'] = $menu_tree;
  }
  $mbase_credits = _mbase_setting('mbase_credits', $theme, NULL);
  $variables['mbase_credits'] = $mbase_credits;
}

/**
 * Build the menu tree from menu name.
 */
function _menuistic_get_menu_tree($menu_name = 'main') {
  $menuTreeParams = new MenuTreeParameters();

  // Get the menu Level.
  $menu_level = _mbase_setting('menu_level', $theme, NULL);

  $menu_level = $menu_level ? $menu_level : '2';
  $menuTreeParams->setMaxDepth($menu_level);

  // Get all other menuisting settings.
  $menu_other_settings = _mbase_setting('menu_other_settings', $theme, NULL);
  $toggle_desc = TRUE;
  if ($menu_other_settings['show_description'] == '0') {
    $toggle_desc = FALSE;
  }
  $hide_orphan_tree = TRUE;
  if ($menu_other_settings['hide_orphan_tree'] == '0') {
    $hide_orphan_tree = FALSE;
  }

  $menu = \Drupal::menuTree()->load($menu_name, $menuTreeParams);
  $data = \Drupal::menuTree()->build($menu);
  _menuistic_menu_add_desc($data['#items'], $toggle_desc, $hide_orphan_tree);

  $data['#attributes']['contentmenu'] = FALSE;
  $data['#attributes']['class'][] = 'content-nav';
  $data['#attributes']['class'][] = 'content-menu';

  return $data;
}

/**
 * Cache the _menuistic_get_menu_tree data, for performance.
 */
function _menuistic_get_cached_menu_tree($menu_name) {
  $cid = 'menuisticmenu' . $menu_name . ':';

  $data = NULL;
  if ($cache = \Drupal::cache()->get($cid)) {
    $data = $cache->data;
  }
  else {
    $data = _menuistic_get_menu_tree($menu_name);
    \Drupal::cache()->set($cid, $data);
  }
  return $data;
}

/**
 * Alter the Menu tree array, add few stuffs.
 */
function _menuistic_menu_add_desc(&$items, $show_desc = 1, $hide_orphan = 1, $isparent = 1) {
  foreach ($items as $key => &$item) {
    if ($hide_orphan && !$item['original_link']->hasChildren && count($item['below']) == 0 && $isparent) {
      unset($items[$key]);
      continue;
    }
    $item['weight'] = $item['original_link']->getWeight();
    $desc = $item['original_link']->getDescription();
    if ($show_desc) {
      $item['descrip'] = $desc ? $desc : FALSE;
    }
    $item['menulink'] = $item['original_link']->getUrlObject()->toString();
    if (count($item['below'])) {
      _menuistic_menu_add_desc($item['below'], $show_desc, $hide_orphan, 0);
      uasort($item['below'], "_menuistic_weight_cmp");
    }
  }
}

/**
 * Comparison function for menu sort.
 */
function _menuistic_weight_cmp(&$a, &$b) {
  if ($a['weight'] == $b['weight']) {
    return 0;
  }
  return ($a['weight'] < $b['weight']) ? -1 : 1;
}
